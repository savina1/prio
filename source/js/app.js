Dropzone.autoDiscover = false;
$(document).ready(function () {


  if ($(window).width() > 767) {
    var lazyloadImages = document.querySelectorAll('img.lazy');
    lazyloadImages.forEach(function (img) {
      img.src = img.dataset.src;
    });
  }

  $(window).resize(function () {
    if ($(window).width() > 767) {
      var lazyloadImages = document.querySelectorAll('img.lazy');
      lazyloadImages.forEach(function (img) {
        img.src = img.dataset.src;
      });
    }
  });





  //search-header bar
  $('.search-header__closed').on('click', function (e) {
    if ($(window).width() < 650) {
      $('.header-top__item--city').hide();
      $('.header-top__item--locations').hide();
    }
    $(this).fadeOut(300);
    $('.search-header__input').addClass('active');
    $('.search-header__button').addClass('active');
  });

  //hide search-header on click anywhere besides the search-header
  $(document).on('click', function (e) {
    if (!$(e.target).hasClass('search-header') && !(e.target).closest('.search-header')) {
      $('.search-header__closed').show();
      $('.search-header__input').removeClass('active');
      $('.search-header__button').removeClass('active');

      if ($(window).width() < 650) {
        $('.header-top__item--city').show();
        $('.header-top__item--locations').show();
      }
    }
  });

  $('.top-nav__item--has-dropdown').on('click', function (e) {
    $(this).toggleClass('opened');
    $('.top-nav__dropdown').toggleClass('active');

  });


  $('.top-nav-xs__active').on('click', function () {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('.top-nav-xs__list').slideDown(300);
    } else {
      $(this).addClass('active');
      $('.top-nav-xs__list').slideUp(300);
    }
  });

  $('.menu-top__mobile-menu').on('click', function () {
    console.log(clickedBtn)


    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('html').removeClass('fixed');
      $('.xs-menu').removeClass('opened');
      console.log('close menu');
    } else {
      $(this).addClass('active');
      $('html').addClass('fixed');
      // console.log('open menu');
      $('.xs-menu').addClass('opened');
    }
    // console.log('click menu');
    // $('html').toggleClass('fixed');
    // $(this).toggleClass('active');
    // $('.xs-menu').toggleClass('opened');

  });


  let clickedBtn = true;

  function mobileMenuInit() {




    // $('.top-nav-xs__active').on('click', function () {
    //   if ($(this).hasClass('active')) {
    //     $(this).removeClass('active');
    //     $('.top-nav-xs__list').slideDown(300);
    //   } else {
    //     $(this).addClass('active');
    //     $('.top-nav-xs__list').slideUp(300);
    //   }
    // });

    //menu tabs
    function initTabs() {
      const triggers = $('[data-tab-trigger]');
      const scenes = $('[data-tab-scene]');

      triggers.on('click', function (e) {

        e.preventDefault();

        const thisTriggerValue = $(this).data('tab-trigger');
        const thisScene = $(`[data-tab-scene=${thisTriggerValue}]`);
        if (!$(this).hasClass('active')) {
          triggers.removeClass('active');

          ///
          $('.bot-nav-xs__right').removeClass('active');
          $('.bot-nav-xs__left').removeClass('active');
          ///

          $('.bot-nav-xs__link').removeClass('active');
          $(this).addClass('active');
          $(this).children('.bot-nav-xs__link').addClass('active');


          scenes.removeClass('active');
          thisScene.addClass('active');
          ///
          $('.bot-nav-xs__left').addClass('active');
          $('.bot-nav-xs__right').addClass('active');
          ///
        }
      });
    }

    initTabs();

    //mobile menu transform main menu back
    $('.xs-menu-slide__back').on('click', function () {
      $('[data-tab-trigger]').removeClass('active');
      $('.bot-nav-xs__right').removeClass('active');
      $('.bot-nav-xs__left').removeClass('active');
    });
  }

  // if ($(window).width() < 1024) {
  mobileMenuInit();
  // }

  $(window).resize(function () {
    if ($(window).width() < 1024) {
      mobileMenuInit();
    }
  });

  //sections tabs
  function initSectionsTabs() {
    const triggers = $('[data-section-tab-trigger]');
    const scenes = $('[data-section-tab-scene]');

    triggers.on('click', function (e) {

      e.preventDefault();

      const thisTriggerValue = $(this).data('section-tab-trigger');
      const thisScene = $(`[data-section-tab-scene=${thisTriggerValue}]`);
      if (!$(this).hasClass('active')) {
        triggers.removeClass('active');
        $(this).addClass('active');
        scenes.removeClass('active');
        thisScene.addClass('active');

      }
    });
  }

  initSectionsTabs();


  //scroll to element inside box (qa)
  function scrollToElement() {
    const triggers = $('[data-scroll-link]');
    const _elements = $('[data-scroll-to-element]');

    triggers.on('click', function (e) {

      e.preventDefault();

      const thisTriggerValue = $(this).data('scroll-link');
      const thisElement = $(`[data-scroll-to-element=${thisTriggerValue}]`);
      const container = $('.card__answers');

      let position = thisElement.position();
      let topPosition = position.top;

      container.animate({scrollTop: container.scrollTop() + topPosition}, 'slow');
      console.log();

    });
  }

  scrollToElement();


  //accordion qa

  (function ($) {
    var tSlide = 250;
    var isAnimated = false;


    var $arrows = $('.show-cell__trigger');

    let wiWidth = $(window).width();

    $(document).on('click', '.accordion__action', function () {
      const $allPanels = $('.accordion__sliding-part');
      const $parent = $(this).parent();
      const $target = $(this).next('.accordion__sliding-part');


      if (!isAnimated) {
        isAnimated = true;
        if (wiWidth < 481) {
          $parent.toggleClass('active');
          $target.slideToggle();

          isAnimated = false;
        } else {

          if (!$parent.hasClass('active')) {

            $allPanels.parent().removeClass('active');

            $allPanels.slideUp(tSlide, function () {
              isAnimated = false;
            });
            $parent.addClass('active');

            $target.slideDown(tSlide);

          } else {

            $parent.removeClass('active');

            $target.slideUp(tSlide, function () {
              isAnimated = false;
            });

          }
        }


      }
    });

  })(jQuery);


  //position footer to bottom of the page
  function footerToBottom() {
    const footer = $('.footer'),
      wrapper = $('.wrapper'),
      footerHeight = footer.innerHeight();
    footer.css({
      marginTop: -footerHeight
    });
    wrapper.css({
      paddingBottom: footerHeight
    });

  }

  footerToBottom();


  $(window).resize(function () {
    footerToBottom();
  });

  // initialize main slider
  if ($('.main-hero').length) {

    var interleaveOffset = 0.5;

    var swiperOptions1 = {
      // Optional parameters
      direction: 'horizontal',
      parallax: true,
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: 'true',
        renderBullet: function (index, className) {
          return `<span class="dot swiper-pagination-bullet">
<svg id="slider-point-1" class="b-slider__point b-slider__point--mobile">
<!--                    <path class="rB" stroke="rgba(0,0,0,0)" d="M10,5c2.8,0,5,2.2,5,5s-2.2,5-5,5s-5-2.2-5-5S7.2,5,10,5z" />-->
                    <path class="rF" stroke="#ffffff" d="M10,5c2.8,0,5,2.2,5,5s-2.2,5-5,5s-5-2.2-5-5S7.2,5,10,5z" />
                </svg>
</span>`;
        }
      },
      speed: 1000,
      autoplay: {
        delay: 7000,
      },
      watchSlidesProgress: true,
      mousewheelControl: true,
      keyboardControl: true

    };

    let slides = $('.swiper-slide.slider-top__item');

    if (slides.length > 1) {
      $('.main-hero__controls').css({'display': 'block'});

      var swiper = new Swiper('.main-hero__container', swiperOptions1);

    }
  }

  //initialize articles slider
  if ($('.articles').length) {

    var swiperOptions2 = {

      slidesPerView: 1,
      slidesPerColumn: 2,
      spaceBetween: 15,

      breakpoints: {
        1025: {
          slidesPerColumn: 1,
          slidesPerView: 2

        }
      },

      pagination: {
        el: '.swiper-bot-pagination',
        clickable: 'true',
        renderBullet: function (index, className) {
          return `<span class="dot swiper-pagination-bullet"><svg><path stroke="#ffffff" d="M10,5c2.8,0,5,2.2,5,5s-2.2,5-5,5s-5-2.2-5-5S7.2,5,10,5z" /></svg></span>`;
        }
      },
      navigation: {
        nextEl: '.swiper-bot-button-next',
        prevEl: '.swiper-bot-button-prev',
      },
    };

    var swiperArticles = new Swiper('.articles__slides', swiperOptions2);

  }


  //initialize product slider
  if ($('.products_swiper').length) {

    var swiperOptions3 = {
      direction: 'horizontal',
      observer: true,
      observeParents: true,
      slidesPerView: 3,
      spaceBetween: 24,
      navigation: {
        nextEl: '.swiper-products-button-next',
        prevEl: '.swiper-products-button-prev',
      },
      breakpoints: {
        320: {
          slidesPerView: 1
        },
        768: {
          slidesPerView: 2
        },
        1200: {
          slidesPerView: 3
        }
      }
    };

    let cards = $('.swiper-slide.prod-card');

    if (cards.length < 4) {
      $('.products__controls').css({'display': 'none'});
    }

    var swiper1 = new Swiper('.products__container', swiperOptions3);
    var swiper2 = new Swiper('.products__container', swiperOptions3);


  }

  //initialize article large slider
  if ($('.large-slider').length) {
    var swiperOptions4 = {
      loop: true,
      navigation: {
        nextEl: '.swiper-button-large-next',
        prevEl: '.swiper-button-large-prev',
      },
      speed: 1000,
      spaceBetween: 30,
      autoplay: {
        delay: 7000,
      },
      slidesPerView: 1,
      mousewheelControl: true,
      keyboardControl: true
    };
    var swiper_new = new Swiper('.large-slider__container', swiperOptions4);
  }


  //initialize article little slider
  if ($('.services').length) {

    var swiperOptions5 = {
      direction: 'horizontal',
      slidesPerView: 3,
      spaceBetween: 25,
      navigation: {
        nextEl: '.swiper-services-button-next',
        prevEl: '.swiper-services-button-prev',
      },
      breakpoints: {
        100: {
          slidesPerView: 1
        },
        768: {
          slidesPerView: 3
        },
        1200: {
          slidesPerView: 6
        }
      }
    };


    var swiper = new Swiper('.services__container_small', swiperOptions5);
  }


  //initialize career tablet slider
  if ($('.tablet-slider').length) {
    var swiperOptions6 = {
      loop: true,
      spaceBetween: 30,
      navigation: {
        nextEl: '.swiper-button-tablet-next',
        prevEl: '.swiper-button-tablet-prev',
      },
      speed: 1000,
      autoplay: {
        delay: 7000,
      },
      slidesPerView: 1,
      mousewheelControl: true,
      keyboardControl: true
    };
    var swiper_tablet_career = new Swiper('.tablet-slider__container', swiperOptions6);
  }


  //initialize career desk slider
  if ($('.desktop-slider').length) {
    var swiperOptions7 = {
      loop: true,
      navigation: {
        nextEl: '.swiper-button-desk-next',
        prevEl: '.swiper-button-desk-prev',
      },
      speed: 1000,
      spaceBetween: 30,
      parallax: true,
      slidesPerView: 1,
      mousewheelControl: true,
      keyboardControl: true
    };
    var swiper_desk_career = new Swiper('.desktop-slider__container', swiperOptions7);
  }


  //initialize career slider
  if ($('.career-heroes').length) {

    var swiperOptions8 = {
      direction: 'horizontal',
      parallax: true,
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: 'true',
        renderBullet: function (index, className) {
          return `<span class="dot swiper-pagination-bullet">
<svg class="b-slider__point b-slider__point--mobile">
                    <path class="rF" stroke="#ffffff" d="M10,5c2.8,0,5,2.2,5,5s-2.2,5-5,5s-5-2.2-5-5S7.2,5,10,5z" />
                </svg>
</span>`;
        }
      },
      speed: 1000,
      autoplay: {
        delay: 7000,
      },
      watchSlidesProgress: true,
      mousewheelControl: true,
      keyboardControl: true

    };

    var swiper_career = new Swiper('.career-heroes__container', swiperOptions8);

  }

  //initialize career reviews slider
  if ($('.reviews').length) {
    var swiperOptions9 = {
      direction: 'horizontal',
      spaceBetween: 16,
      slidesPerView: 'auto',
      parallax: true,
      loop: true,
      navigation: {
        nextEl: '.swiper-button-career-next',
        prevEl: '.swiper-button-career-prev',
      },
      speed: 1000,
      autoplay: {
        delay: 7000,
      },
      watchSlidesProgress: true,
      mousewheelControl: true,
      keyboardControl: true,
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
          spaceBetween: 20,
          centeredSlides: false
        },
        // when window width is >= 640px
        768: {
          centeredSlides: true
        }
      }
    };

    var swiper_career_reviews = new Swiper('.reviews__container', swiperOptions9);

    $('[data-lity]').on('click', function (e) {
      swiper_career_reviews.autoplay.stop();
    });
  }


  //scroll to top
  $('.light-footer__to-top').on('click', function () {
    $('html, body').animate({scrollTop: 0}, 'slow');
  });


  //tooltip
  $('.tooltip').tooltipster({
    theme: ['tooltipster-shadow', 'tooltipster-shadow-customized'],
    interactive: 'true',
    autoClose: 'false',
    trigger: 'custom',


    triggerOpen: {
      mouseenter: true,
      touchstart: true
    },

    triggerClose: {
      mouseleave: true,
      scroll: true
    },
    contentCloning: true,

    functionInit: function (instance, helper) {
      var content = $(helper.origin).find('.tooltip_content').detach();
      instance.content(content);
    },

    // if you use a single element as content for several tooltips, set this option to true
    // functionReady: function () {
    //   $('.tooltip_close').click(function () {
    //     $('.tooltip').tooltipster('hide');
    //   });
    // },

  });

  let popupTexts = [{
    id: 0,
    title: 'Доп. офис  “Операционная касса №1”',
    phone: '434 34 34 34',
    address: 'sdsdsd sd sdsd   address'

  },
    {
      id: 1,
      title: 'Доп. офис  “Операци 444444”',
      phone: '99999',
      address: '2 sdsdsd sd sdsd   address'

    },];

  const popup = $('.map-popup');

  //Yandex Maps
  if ($('#YMapsID').length) {
    ymaps.ready(function () {
      let baloons = [{
        tabTrigger: 1,
        id: 0,
        coords: [54.627191, 39.746780],
        options: [
          {
            // hintContent: 'Москва',
          },
          {
            iconLayout: 'default#imageWithContent',
            iconImageHref: 'assets/img/location-icon.svg',
            iconImageSize: [24, 40],
            iconImageOffset: [-18, -43],
            iconContentOffset: [15, 15],
          }
        ]
      },
        {
          tabTrigger: 2,
          id: 1,
          coords: [54.637191, 39.746780],
          options: [
            {
              // hintContent: 'Тобольск',
            },
            {
              iconLayout: 'default#imageWithContent',
              iconImageHref: 'assets/img/location-icon.svg',
              iconImageSize: [24, 40],
              iconImageOffset: [-18, -43],
              iconContentOffset: [15, 15],
            }
          ]
        },
        {
          tabTrigger: 3,
          id: 2,
          coords: [56.150670, 44.206751],
          options: [
            {
              // hintContent: 'Кстово',
            },
            {
              iconLayout: 'default#imageWithContent',
              iconImageHref: 'assets/img/location-icon.svg',
              iconImageSize: [24, 40],
              iconImageOffset: [-18, -43],
              iconContentOffset: [15, 15],
            }
          ]
        }
      ];

      let myMap = new ymaps.Map('YMapsID', {
        center: [54.627191, 39.746780],
        zoom: 14,
        controls: [],
      });

      //zoom controls
      // ZoomLayout = ymaps.templateLayoutFactory.createClass('<div>' +
      //   '<div id=\'zoom-in\' class=\'btn\'><i class=\'icon-plus\'><img src=\'assets/img/plus.svg\'></i></div>' +
      //   '<div id=\'zoom-out\' class=\'btn\'><i class=\'icon-minus\'><img src=\'assets/img/minus.svg\'></i></div>' +
      //   '</div>', {
      //
      //   build: function () {
      //     ZoomLayout.superclass.build.call(this);
      //
      //     this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
      //     this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);
      //
      //     $('#zoom-in').bind('click', this.zoomInCallback);
      //     $('#zoom-out').bind('click', this.zoomOutCallback);
      //   },
      //
      //   clear: function () {
      //     $('#zoom-in').unbind('click', this.zoomInCallback);
      //     $('#zoom-out').unbind('click', this.zoomOutCallback);
      //
      //     ZoomLayout.superclass.clear.call(this);
      //   },
      //
      //   zoomIn: function () {
      //     var map = this.getData().control.getMap();
      //     map.setZoom(map.getZoom() + 1, {checkZoomRange: true});
      //   },
      //
      //   zoomOut: function () {
      //     var map = this.getData().control.getMap();
      //     map.setZoom(map.getZoom() - 1, {checkZoomRange: true});
      //   }
      // }),
      // zoomControl = new ymaps.control.ZoomControl({options: {layout: ZoomLayout}});

      baloons.forEach((el, i) => {
        console.log(el);
        let item = new ymaps.Placemark(el.coords, {...el.options[0]}, {...el.options[1]});
        myMap.geoObjects.add(item);

        const mapTriggers = $('[data-tab-trigger]');
        const thisMapTrigger = $(`[data-tab-trigger=${el.tabTrigger}]`);

        item.events.add(['click'], function (e) {

          //find ищет первый подходящий по условию элемент
          const curElem = popupTexts.find(item => item.id == el.id);
          // console.log(el.id);
          popup.find('.tel').text(curElem.phone);
          popup.find('.address').text(curElem.address);
          // console.log(popupTexts.find(item => item.id == el.id));
          popup.addClass('active');

          // console.log("CLICK");

          mapTriggers.removeClass('active');

          thisMapTrigger.addClass('active');
          e.get('target').options.set('iconImageHref', 'assets/img/location-icon-active.svg');

          const mapPopup = $('.map-popups__popup');

          const closePopup = () => {
            $('.map-popups__popup').removeClass('active');
            e.get('target').options.set('iconImageHref', 'assets/img/location-icon.svg');
          };

          const setPopupBottom = value => {
            mapPopup.css('bottom', value);
          };

          const popupPositions = {
            full: 0,
            peeking: -300,
            hidden: -mapPopup.outerHeight()
          };
          if ($(window).width() < 767 && $('.map-popups__drag-down').length) {
            setPopupBottom(popupPositions.peeking);

            // "Истиное" значение bottom элемента, к которому прибавляется смещение при перетаскивании
            let posY = popupPositions.peeking;
            // Позиция начала перетаскивания, относительно которой вычисляется смещение
            let startY;
            // "Текущая" позиция элемента при перетаскивании. При 'touchend' ее значение присваивается "истиному"
            let y = posY;

            // Выбираем ближайшее "стабильное" положение и переходим к нему
            const takePopupPosition = value => {
              let minDiff = Infinity;
              let minPosName;

              for (const posName of Object.keys(popupPositions)) {
                const diff = Math.abs(popupPositions[posName] - value);
                // console.log(posName, diff);
                if (diff < minDiff) {
                  minDiff = diff;
                  minPosName = posName;
                }
              }

              const newPosY = popupPositions[minPosName];

              mapPopup.animate({
                bottom: newPosY
              }, 0.25, 'swing', () => {
                posY = newPosY;

                if (minPosName === 'hidden') {
                  closePopup();
                }
              });
            };

            mapPopup.on('touchstart', e => {
              const touch = e.touches[0];

              startY = touch.screenY;

              // console.log("posY", posY);
              // console.log("startY", startY);

              e.stopPropagation();
              // e.stopImmediatePropagation();
              e.preventDefault();
              // return false;
            });

            mapPopup.on('touchmove', e => {
              const touch = e.touches[0];

              // console.log("posY", posY);

              y = posY - (touch.screenY - startY);
              y = Math.min(0, y);
              y = Math.max(-mapPopup.outerHeight(), y);

              setPopupBottom(y);

              // console.log(y);
            });

            mapPopup.on('touchend', e => {
              // console.log("posY = y", y);

              setPopupBottom(posY = y);

              takePopupPosition(posY);
            });

            mapPopup.on('touchcancel', e => {
              setPopupBottom(posY);
            });
          }

          $('.map-popups__close-btn--white-left').click(closePopup);
        });
      });

      // myMap.controls.add(zoomControl, {position: {right: 5, bottom: 40}});

    });
  }


  //validation
  jQuery.validator.addMethod('mobileRu', function (value, el) {
    const phRe = /^\+7\(([0-9]{3})\) ([0-9]{3})\-([0-9]{4})$/;
    // console.log("log", value.match(phRe));
    return !!value.match(phRe);
  }, 'Введите корректный номер телефона');

  $('.phone_number').inputmask('+7(999) 999-9999');
  $('#cardNumber').inputmask('9999 9999 9999 9999');
  $('#cvv').inputmask('999');
  $('#month').inputmask('99');
  $('#year').inputmask('99');
  $('#cardNumberTo').inputmask('9999 9999 9999 9999');
  $('#inn').inputmask('9999999999');
  $('#innIs').inputmask('9999999999');
  Inputmask({'mask': '999[99]', placeholder: ''}).mask($('#sumTransfer'));
  // $('#sumTransfer').inputmask('999[99]');

  $('#request').validate({
    submitHandler: function (form) {
      let thisForm = $('#request');

      thisForm.hide(0, function (e) {
        thisForm.closest('.request__card').addClass('with-thankyou');
        thisForm.next('.request__thankyou').fadeIn(300);
      });

    },

    rules: {
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      numberInput: {
        required: true,
        // phoneUS: true
        mobileRu: true
      }

    },

    messages: {
      username: {
        required: 'Поле обязательно для заполнения',
      },

      numberInput: {
        required: 'Поле обязательно для заполнения',
        // phoneUS:'Введите телефон по формату'
        mobileRu: 'Введите корректный номер телефона'
      },

      email: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      }
    }
  });


  $('#reserve').validate({
    submitHandler: function (form) {
      let thisForm = $('#reserve');

      thisForm.hide(0, function (e) {
        thisForm.closest('.request__card').addClass('with-thankyou');
        thisForm.next('.request__thankyou').fadeIn(300);
      });

    },

    rules: {
      innIs: {
        required: true
      },
      orgNameIs: {
        required: true
      },
      fio: {
        required: true
      },
      emailInput: {
        required: true,
        email: true
      },
      numberInput: {
        required: true,
        mobileRu: true
      }

    },

    messages: {
      innIs: {
        required: 'Поле обязательно для заполнения',
      },
      orgNameIs: {
        required: 'Поле обязательно для заполнения',
      },
      fio: {
        required: 'Поле обязательно для заполнения',
      },
      numberInput: {
        required: 'Поле обязательно для заполнения',
        mobileRu: 'Введите корректный номер телефона'
      },
      emailInput: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      }
    }
  });

  $('#feedback-page').validate({
    submitHandler: function (form) {
      let thisForm = $('#feedback-page');

      thisForm.hide(0, function (e) {
        thisForm.closest('.request__card').addClass('with-thankyou');
        thisForm.next('.request__thankyou').fadeIn(300, function () {
          $('html, body').animate({scrollTop: $(this).offset().top - 200}, 'slow');
        });
      });

    },

    rules: {
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      numberInput: {
        required: true,
        // phoneUS: true
        mobileRu: true
      },
      addressInput: {
        required: true,
      },
      goal: {
        required: true,
      },
      textMess: {
        required: true,
      }

    },

    messages: {
      username: {
        required: 'Поле обязательно для заполнения',
      },

      numberInput: {
        required: 'Поле обязательно для заполнения',
        mobileRu: 'Введите корректный номер телефона'
      },

      email: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      },
      addressInput: {
        required: 'Поле обязательно для заполнения'
      },
      goal: {
        required: 'Поле обязательно для заполнения'
      },
      textMess: {
        required: 'Поле обязательно для заполнения'
      }
    }
  });

  $('#request1').validate({
    submitHandler: function (form) {
      let thisForm = $('#request1');

      // let isActive = false;
      thisForm.hide(0, function (e) {
        thisForm.closest('.request__card').addClass('with-thankyou');
        thisForm.next('.request__thankyou').fadeIn(300);
      });
    },

    rules: {
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      numberInput: {
        required: true,
        // phoneUS: true
        mobileRu: true
      }

    },

    messages: {
      username: {
        required: 'Поле обязательно для заполнения',
      },

      numberInput: {
        required: 'Поле обязательно для заполнения',
        mobileRu: 'Введите корректный номер телефона'
      },

      email: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      }
    }
  });


  //vacancies form popup
  $('#Vrequest').validate({
    submitHandler: function (form) {
      let thisForm = $('#Vrequest');

      thisForm.hide(0, function (e) {
        thisForm.closest('.request__card').addClass('with-thankyou');
        thisForm.next('.request__thankyou').fadeIn(300);
      });

    },

    rules: {
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      numberInput: {
        required: true,
        // phoneUS: true
        mobileRu: true
      }

    },

    messages: {
      username: {
        required: 'Поле обязательно для заполнения',
      },

      numberInput: {
        required: 'Поле обязательно для заполнения',
        // phoneUS:'Введите телефон по формату'
        mobileRu: 'Введите корректный номер телефона'
      },

      email: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      }
    }
  });

  $('#Vfeedback-page').validate({
    submitHandler: function (form) {
      let thisForm = $('#Vfeedback-page');

      thisForm.hide(0, function (e) {
        thisForm.closest('.request__card').addClass('with-thankyou');
        thisForm.next('.request__thankyou').fadeIn(300, function () {
          $('html, body').animate({scrollTop: $(this).offset().top - 200}, 'slow');
        });
      });

    },

    rules: {
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      numberInput: {
        required: true,
        // phoneUS: true
        mobileRu: true
      },
      addressInput: {
        required: true,
      },
      goal: {
        required: true,
      },
      textMess: {
        required: true,
      }

    },

    messages: {
      username: {
        required: 'Поле обязательно для заполнения',
      },

      numberInput: {
        required: 'Поле обязательно для заполнения',
        mobileRu: 'Введите корректный номер телефона'
      },

      email: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      },
      addressInput: {
        required: 'Поле обязательно для заполнения'
      },
      goal: {
        required: 'Поле обязательно для заполнения'
      },
      textMess: {
        required: 'Поле обязательно для заполнения'
      }
    }
  });


  $('#moneyTransfer').validate({
    submitHandler: function (form) {
    },

    rules: {
      cardNumber: {
        required: true
      },
      dateMonth: {
        required: true
      },
      dateYear: {
        required: true
        // mobileRu: true
      }, cvv: {
        required: true
      },
      cardNumberTo: {
        required: true
      },
      sumTransfer: {
        required: true,
        min: 100,
        max: 70000
      },
    },

    messages: {
      cardNumber: {
        required: 'Поле обязательно для заполнения',
      },

      dateMonth: {
        required: 'Поле обязательно для заполнения',
        // phoneUS:'Введите телефон по формату'
        mobileRu: 'Введите корректный номер телефона'
      },

      dateYear: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      },
      sumTransfer: {
        required: '',
        min: 'Минимальная сумма 100',
        max: 'Максимальная сумма 70000'
      }
    }
  });


  $('#job-request').validate({
    submitHandler: function (form) {
      let thisForm = $('#job-request');

      // let isActive = false;
      thisForm.hide(0, function (e) {
        thisForm.closest('.pink-form__closest').addClass('with-thankyou');
        thisForm.next('.pink-form__thankyou').fadeIn(300);
      });
    },

    rules: {
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      numberInput: {
        required: true,
        mobileRu: true
      },
      select: {
        required: true
      }

    },

    messages: {
      username: {
        required: 'Поле обязательно для заполнения',
      },

      numberInput: {
        required: 'Поле обязательно для заполнения',
        // phoneUS:'Введите телефон по формату'
        mobileRu: 'Введите корректный номер телефона'
      },

      email: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      }
    }
  });

  $('#open-deposit-request').validate({
    submitHandler: function (form) {
      let thisForm = $('#open-deposit-request');

      thisForm.hide(0, function (e) {
        thisForm.closest('.pink-form__closest').addClass('with-thankyou');
        thisForm.next('.pink-form__thankyou').fadeIn(300);
      });
    },

    rules: {
      username: {
        required: true
      },
      inn: {
        required: true
      },
      orgName: {
        required: true
      },
      numberInput: {
        required: true,
        mobileRu: true
      },
    },

    messages: {
      username: {
        required: 'Поле обязательно для заполнения',
      },

      numberInput: {
        required: 'Поле обязательно для заполнения',
        mobileRu: 'Введите корректный номер телефона'
      },

      inn: {
        required: 'Поле обязательно для заполнения'
      },
      orgName: {
        required: 'Поле обязательно для заполнения'
      }

    }
  });


  $('#sumTransfer').on('input', function (e) {
    let total = $('#sumTransferTotal');
    let sum = parseInt($(this).val()) || 0;
    let sumTotal;
    let calculations = sum * 1.5 / 100;
    let comission = $('.actions-card__sum');
    total.val('');
    comission.text(39);
    if (sum > 99 && sum < 70001) {
      if (calculations < 39) {
        sumTotal = sum + 39;
        // comission.text(39)
      } else {
        comission.text(calculations.toFixed(2));
        sumTotal = (calculations + sum).toFixed(2);
      }
      total.val(sumTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') || '');
    }
  });


  //Simple Dropzonejs
  $('#myDropzone').dropzone({
    url: '/upload',
    addRemoveLinks: true,
    createImageThumbnails: false,
    maxFiles: 1,
    maxFilesize: 30, // MB

    dictFileTooBig: 'Слишком большой файл. Максимальный размер: {{maxFilesize}}МБ.',
    dictInvalidFileType: 'Данный тип файлов не поддерживается', // Default: You can't upload files of this type.
    dictResponseError: 'Ошибка на стороне сервера', // Default: Server responded with {{statusCode}} code.
    dictCancelUpload: null, // Default: Cancel upload
    dictUploadCanceled: null, // Default: Upload canceled.
    dictCancelUploadConfirmation: '', // Default: Are you sure you want to cancel this upload?
    dictRemoveFile: '', // Default: Remove file
    dictMaxFilesExceeded: 'Звгрузить можно только один файл.', // Default: You can not upload any more files.
    dictFileSizeUnits: {gb: 'ГБ', mb: 'МБ', kb: 'Кб', b: 'б'},

    acceptedFiles: 'application/pdf, .pdf, .doc, .docx',

    success: function (file, response) {
      var imgName = response;
      file.previewElement.classList.add('dz-success');
      console.log('Successfully uploaded :' + imgName);
    },
    // error: function (file, response) {
    //   file.previewElement.classList.add("dz-error");
    // },
    init: function (e) {
      this.on('complete', function (file) {
        $('.dz-remove').html('<div><img src=\'../assets/img/cross_close.svg\'></div>');
        $('.dz-success-mark').html('<div><img src=\'../assets/img/ok-msg.svg\'></div>');
        $('.dz-error-mark').html('<div><img src=\'../assets/img/error-msg.svg\'></div>');
        $('.dz-progress').html('<div><img src=\'../assets/img/spinner.svg\'></div>');
      });
      this.on('addedfile', function () {
        if (this.files[1] != null) {
          this.removeFile(this.files[0]);
        }
        $('#resume_url_address').hide();
      });


      this.on('removedfile', function (file) {
        $('#resume_url_address').show();
      });
    },
  });


  //Simple Dropzonejs
  $('#feedbackMyDropzone').dropzone({
    url: '/upload',
    addRemoveLinks: true,
    createImageThumbnails: false,
    maxFiles: 9,
    maxFilesize: 7, // MB

    dictFileTooBig: 'Слишком большой файл. Максимальный размер: {{maxFilesize}}МБ.', // Default: File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.
    dictInvalidFileType: 'Данный тип файлов не поддерживается', // Default: You can't upload files of this type.
    dictResponseError: 'Ошибка на стороне сервера', // Default: Server responded with {{statusCode}} code.
    dictCancelUpload: null, // Default: Cancel upload
    dictUploadCanceled: null, // Default: Upload canceled.
    dictCancelUploadConfirmation: '', // Default: Are you sure you want to cancel this upload?
    dictRemoveFile: '', // Default: Remove file
    dictFileSizeUnits: {gb: 'ГБ', mb: 'МБ', kb: 'Кб', b: 'б'},

    acceptedFiles: 'application/pdf, .pdf, .doc, .docx, .txt, .jpeg, .rtf, .png, .jpg',

    success: function (file, response) {
      var imgName = response;
      file.previewElement.classList.add('dz-success');
      console.log('Successfully uploaded :' + imgName);
    },
    init: function (e) {
      this.on('complete', function (file) {
        $('.dz-remove').html('<div><img src=\'../assets/img/cross_close.svg\'></div>');
        $('.dz-success-mark').html('<div><img src=\'../assets/img/ok-msg.svg\'></div>');
        $('.dz-error-mark').html('<div><img src=\'../assets/img/error-msg.svg\'></div>');
        $('.dz-progress').html('<div><img src=\'../assets/img/spinner.svg\'></div>');
      });
      this.on('addedfile', function () {
        $('.dz-message').show();
        if (this.files[9] != null) {
          this.removeFile(this.files[9]);
        }
      });
    },
  });

  //Reserve Dropzonejs
  $('#reserveMyDropzone').dropzone({
    url: '/upload',
    addRemoveLinks: true,
    createImageThumbnails: false,
    maxFiles: 9,
    maxFilesize: 7, // MB

    dictFileTooBig: 'Слишком большой файл. Максимальный размер: {{maxFilesize}}МБ.', // Default: File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.
    dictInvalidFileType: 'Данный тип файлов не поддерживается', // Default: You can't upload files of this type.
    dictResponseError: 'Ошибка на стороне сервера', // Default: Server responded with {{statusCode}} code.
    dictCancelUpload: null, // Default: Cancel upload
    dictUploadCanceled: null, // Default: Upload canceled.
    dictCancelUploadConfirmation: '', // Default: Are you sure you want to cancel this upload?
    dictRemoveFile: '', // Default: Remove file
    dictFileSizeUnits: {gb: 'ГБ', mb: 'МБ', kb: 'Кб', b: 'б'},

    acceptedFiles: 'application/pdf, .pdf, .doc, .docx',

    success: function (file, response) {
      var imgName = response;
      file.previewElement.classList.add('dz-success');
      console.log('Successfully uploaded :' + imgName);
    },
    init: function (e) {
      this.on('complete', function (file) {
        $('.dz-remove').html('<div><img src=\'../assets/img/cross_close.svg\'></div>');
        $('.dz-success-mark').html('<div><img src=\'../assets/img/ok-msg.svg\'></div>');
        $('.dz-error-mark').html('<div><img src=\'../assets/img/error-msg.svg\'></div>');
        $('.dz-progress').html('<div><img src=\'../assets/img/spinner.svg\'></div>');
      });
      this.on('addedfile', function () {
        $('.dz-message').show();
        if (this.files[9] != null) {
          this.removeFile(this.files[4]);
        }
      });
    },
  });


  //vacancies popup Dropzonejs
  $('#VmyDropzone').dropzone({
    url: '/upload',
    addRemoveLinks: true,
    createImageThumbnails: false,
    maxFiles: 1,
    maxFilesize: 30, // MB
    dictFileTooBig: 'Слишком большой файл. Максимальный размер: {{maxFilesize}}МБ.', // Default: File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.
    dictInvalidFileType: 'Данный тип файлов не поддерживается', // Default: You can't upload files of this type.
    dictResponseError: 'Ошибка на стороне сервера', // Default: Server responded with {{statusCode}} code.
    dictCancelUpload: null, // Default: Cancel upload
    dictUploadCanceled: null, // Default: Upload canceled.
    dictCancelUploadConfirmation: '', // Default: Are you sure you want to cancel this upload?
    dictRemoveFile: '', // Default: Remove file
    dictMaxFilesExceeded: 'Звгрузить можно только один файл.', // Default: You can not upload any more files.
    dictFileSizeUnits: {gb: 'ГБ', mb: 'МБ', kb: 'Кб', b: 'б'},
    acceptedFiles: 'application/pdf, .pdf, .doc, .docx',

    success: function (file, response) {
      var imgName = response;
      file.previewElement.classList.add('dz-success');
      console.log('Successfully uploaded :' + imgName);
    },
    init: function (e) {
      this.on('complete', function (file) {
        $('.dz-remove').html('<div><img src=\'../assets/img/cross_close.svg\'></div>');
        $('.dz-success-mark').html('<div><img src=\'../assets/img/ok-msg.svg\'></div>');
        $('.dz-error-mark').html('<div><img src=\'../assets/img/error-msg.svg\'></div>');
        $('.dz-progress').html('<div><img src=\'../assets/img/spinner.svg\'></div>');
      });
      this.on('addedfile', function () {
        if (this.files[1] != null) {
          this.removeFile(this.files[0]);
        }
        $('#Vresume_url_address').hide();
      });


      this.on('removedfile', function (file) {
        $('#Vresume_url_address').show();
      });
    },
  });


  //management popups
  let triggersManagement = $('.prod-card_management');
  triggersManagement.on('click', function (e) {
    e.preventDefault();

    const thisName = $(this).find('.prod-card__name').html();
    const thisLastName = $(this).find('.prod-card__name_last').html();
    const thisJob = $(this).find('.prod-card__text').html();
    const thisDescr = $(this).find('.prod-card__hidden-descr-popup').html();
    const thisPic = $(this).find('.prod-card__img img').attr('src');
    let isActive = false;
    const thisForm = $('.popup');
    const thisFormDescr = thisForm.find('.popup__descr');
    thisForm.find('.popup__name').text(thisName);
    thisForm.find('.popup__name_last').text(thisLastName);
    thisForm.find('.popup__job').text(thisJob);
    thisFormDescr.html(thisDescr || '');
    thisForm.find('.popup__left').css('background-image', `url("${thisPic}")`);
    const thisOverlay = thisForm.closest('.overlay');
    thisForm.show();
    thisOverlay.fadeIn(200);
    thisForm.addClass('active');
    isActive = !isActive;
    disableBodyScroll(isActive, '.popup__descr');
    $('html').addClass('fixed');
    if (thisFormDescr.prop('scrollHeight') > thisFormDescr.innerHeight()) {
      thisFormDescr.css('overflow-y', 'scroll');
    }
  });


  //all tarifs popup
  $('#reserve-bill').on('click', function (e) {
    let isActive = false;
    const thisForm = $('.popup');
    const thisOverlay = thisForm.closest('.overlay');

    thisForm.show();
    thisOverlay.fadeIn(200);
    thisForm.addClass('active');
    isActive = !isActive;
    disableBodyScroll(isActive, '.popup__container_request');
    $('html').addClass('fixed');

  })

  //business ip popup
  $('.rko__button--strawberry').on('click', function (e) {
    e.preventDefault();
    let isActive = false;
    const thisForm = $('.popup');
    const thisOverlay = thisForm.closest('.overlay');

    thisForm.show();
    thisOverlay.fadeIn(200);
    thisForm.addClass('active');
    isActive = !isActive;
    disableBodyScroll(isActive, '.popup__container_request');
    $('html').addClass('fixed');

  })


  //vacancy popup
  let triggersVacancy = $('.vacancy__btn_popup');
  triggersVacancy.on('click', function (e) {
    e.preventDefault();

    let isActive = false;
    const thisForm = $('.popup');
    const thisOverlay = thisForm.closest('.overlay');
    thisForm.show();
    thisOverlay.fadeIn(200);
    thisForm.addClass('active');
    isActive = !isActive;
    disableBodyScroll(isActive, '.popup__container');
    $('html').addClass('fixed');
  });

  if ($(window).width() < 768) {
    if ($('.consumer-credit__inner-btn').length) {
      let isActive = false;
      //open popup
      isActive = !isActive;
      disableBodyScroll(isActive, '.popup__descr');
    }
  }


  $(document).on('click', '.overlay', function (e) {
    if ($(this).is(e.target))
      closePopup($(this));
  });

  $(document).on('click', '.popup__close-btn', function (e) {
    if ($(this).is(e.target))
      closePopup($(this));
  });

  $('.popup').on('click', function (e) {
    e.stopPropagation();
  });

  $('.popup__close-btn').click(function (e) {
    // $('.overlay').fadeOut(200)
    // $(this).parent().parent().fadeOut(200)
    $(this).closest('.overlay').fadeOut(200);
    $('html').removeClass('fixed');
    $(this).closest('.popup').removeClass('active');
    $('.popup__descr').css('overflow-y', 'hidden');
  });

  function closePopup(el) {
    var parent = el.closest('.overlay');
    parent.fadeOut(200);
    $('html').removeClass('fixed');
    el.closest('.popup').removeClass('active');
    $('.popup__descr').css('overflow-y', 'hidden');
  }


  //radio buttons
  $('.radio label').click(function () {
    $('.table__row').removeClass('active');
    if ($('input[type=radio]').is(':checked')) {
      $(this).closest('.table__row').addClass('active');
    }
  });

  //show documents on tablet v
  $('.documents__open-other').on('click', function () {

    if ($(window).width() < 768) {
      $('.documents__item:nth-of-type(1n+3)').fadeIn(300);
    }
    $('.documents__item:nth-of-type(1n+5)').fadeIn(300);

    $(this).fadeOut(300);

  });


  var scenes = document.getElementsByClassName('scene');
  let masik = [];
  $.each(scenes, function (index, value) {
    masik[index] = new Parallax(scenes[index], {
      clipRelativeInput: true,
      hoverOnly: true
    });
  });

  // select plugin
  $('.pink-form__select2').select2({
    placeholder: 'Направление',
    // theme: 'opaque',
    width: '100%',
    'language': {
      'noResults': function () {
        return 'Совпадений не найдено';
      }
    }
  });


  //dropzone link of file drop
  function hideDropzone() {
    let isLinkActive = false;
    if ($('#resume_url_address').val() !== '') {
      isLinkActive = true;
    } else {
      isLinkActive = false;
    }
    if (isLinkActive) {
      // $('.dz-message').addClass('hidden')
      $('.dz-message').slideUp(300);
      $('.dropzone__url').addClass('drop-hidden');
      $('.dropzone').addClass('hidden');
    } else {
      $('.dz-message').slideDown(300);
      $('.dropzone__url').removeClass('drop-hidden');
      $('.dropzone').removeClass('hidden');
    }
  }

  $('#resume_url_address').on('input', function (e) {
    hideDropzone();
  });

  $('.dropzone .dz-preview .dz-remove').on('click', function (e) {
    e.preventDefault();
    console.log('ssssss');
    $('#resume_url_address').show();
  });


  //vacancies cards flip
  if ($('.vacancies').length) {
    let vacanciesHeight = $('.front .inner');

    // console.log(vacanciesHeight);
    function fitContent() {
      vacanciesHeight.each(function () {
        $(this).closest('.content').css({
          height: $(this).height()
        });
      });
    }

    fitContent();

    $(window).resize(function () {
      $('.requirements__item').css('width', 'auto');
      console.log('resizez');
      fitContent();
    });


    $('.vacancy__btn_conditions').on('click', function (e) {
      let thisVacancy = $(this).closest('.vacancy');
      let thisContent = thisVacancy.find('.content');

      if (thisVacancy.find('input').is(':checked')) {
        $(window).resize(function () {
          const height = thisVacancy.find('.front .inner').height();
          setTimeout(function () {
            thisContent.css({
              height
            });
          }, 600);
        });

        const height = thisVacancy.find('.front .inner').height();
        setTimeout(function () {
          thisContent.css({
            height
          });
        }, 600);


      } else {
        $(window).resize(function () {
          const height = thisVacancy.find('.back .inner').height();
          setTimeout(function () {
            thisContent.css({
              height
            });
          }, 600);
        });

        const height = thisVacancy.find('.back .inner').height();
        setTimeout(function () {
          thisContent.css({
            height
          });
        }, 600);
      }
    });
  }

  //atm map popup
  $('.ymap__menu').on('click', function () {
    $('.overlay').fadeIn(300);
    $('.search-news_atm').addClass('active');
  });

  $('.search-news__back').on('click', function () {
    $('.overlay').fadeOut(300);
    $('.search-news_atm').removeClass('active');
  });
});


var disableBodyScroll = (function () {

  var _selector = false,
    _element = false,
    _clientY;

  if (!Element.prototype.matches)
    Element.prototype.matches = Element.prototype.msMatchesSelector ||
      Element.prototype.webkitMatchesSelector;

  if (!Element.prototype.closest)
    Element.prototype.closest = function (s) {
      var ancestor = this;
      if (!document.documentElement.contains(el)) return null;
      do {
        if (ancestor.matches(s)) return ancestor;
        ancestor = ancestor.parentElement;
      } while (ancestor !== null);
      return el;
    };


  var preventBodyScroll = function (event) {
    if (false === _element || !event.target.closest(_selector)) {
      event.preventDefault();
    }
  };

  var captureClientY = function (event) {
    // only respond to a single touch
    if (event.targetTouches.length === 1) {
      _clientY = event.targetTouches[0].clientY;
    }
  };

  var preventOverscroll = function (event) {
    // only respond to a single touch
    if (event.targetTouches.length !== 1) {
      return;
    }

    var clientY = event.targetTouches[0].clientY - _clientY;

    // The element at the top of its scroll,
    // and the user scrolls down
    if (_element.scrollTop === 0 && clientY > 0) {
      event.preventDefault();
    }

    // The element at the bottom of its scroll,
    // and the user scrolls up
    // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
    if ((_element.scrollHeight - _element.scrollTop <= _element.clientHeight) && clientY < 0) {
      event.preventDefault();
    }

  };

  /**
   * Disable body scroll. Scrolling with the selector is
   * allowed if a selector is porvided.
   *
   * @param  boolean allow
   * @param  string selector Selector to element to change scroll permission
   * @return void
   */
  return function (allow, selector) {
    if (typeof selector !== 'undefined') {
      _selector = selector;
      _element = document.querySelector(selector);
    }

    if (true === allow) {
      if (false !== _element) {
        _element.addEventListener('touchstart', captureClientY, false);
        _element.addEventListener('touchmove', preventOverscroll, false);
      }
      document.body.addEventListener('touchmove', preventBodyScroll, false);
    } else {
      if (false !== _element) {
        _element.removeEventListener('touchstart', captureClientY, false);
        _element.removeEventListener('touchmove', preventOverscroll, false);
      }
      document.body.removeEventListener('touchmove', preventBodyScroll, false);
    }
  };
}());
