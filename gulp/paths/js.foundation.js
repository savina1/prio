'use strict';

module.exports = [
  './node_modules/jquery/dist/jquery.min.js',
  './node_modules/swiper/js/swiper.min.js',
  './node_modules/tooltipster/dist/js/tooltipster.bundle.js',
  './node_modules/inputmask/dist/jquery.inputmask.js',
  './node_modules/jquery-validation/dist/jquery.validate.js',
  './node_modules/lity/dist/lity.js',
  './node_modules/parallax-js/dist/parallax.js',
  './node_modules/dropzone/dist/dropzone.js',
  './node_modules/select2/dist/js/select2.js'
];
